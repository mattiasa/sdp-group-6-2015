![Alt text](https://bytebucket.org/mattiasa/sdp-group-6-2015/raw/62e0449f985c497d7c7bd13b8a424cc8920d07f9/kernel_panic.jpg)

### Winners of the best vision system award 2015! ###

Launching the program
==================

Launch our code you need to run the launch.py script.

The script should be run using the following command:

`python launch.py [options] pitch side colour serial-port role`

options:
-q  - skip the 10 second delay when starting the program (used to let the comms settle)

pitch  [0,1] 
0 if we are on the main pitch and 1 in the side room. 

side [left, right]
says what side of the pitch we are on. 
This can sometimes be somewhat counter intuitive so make sure you have put in the right side. 

colour [blue, yellow]
colour is completely useless and will be removed in a later version, however it is required to run at this moment in time.

serial-port /path/to/port
Is the path to the radio stick port. 
The port can be found in /dev (use $ls /dev to find it)
It is usually either /dev/ttyACM0 or dev/ttyACM1

role [attacker,defender]
Decides which set of plans we should execute. 
Defender is the pass ball plan while attacker is the align and shoot at goal plans.
This currently does nothing. The attacker position and plan will always be chosen. 


####Running the simulator
NOTE: The simulator was broken by recent changes to the codebase and it has not yet been updated.

Run the simulator using
    python Simulate.py
    
The starting positions of the robots can be changed in Simulate.py
If using 32 bit python (not on DICE), the SHOWLABELS flag can be changed to True. This allows drawing planning state.

Other simulator variables can be changed in Simulator/Simulator.py


####Example how to launch program

python launch.py -q 0 right blue /dev/ttyACM0 attack

python launch.py -q 0 left blue /dev/ttyACM0 attack

python launch.py -q 0 left blue /dev/ttyACM0 attack


####Calibrations and starting communications

Communications are switched of by default. 
If you wish to switch them on, simply press the "c" key while having the GUI open and program running. 

Calibration can be done for different parts. 
Specifically there is a "plate" mask for the position of the robot, a "dot" mask for the orientation of the robot 
(by finding the position of the dot in relation to the rest of the plate) and a "red" mask that detects the red ball. 
Each of these can be accessed and configured by pressing the "p", "d", and "r" keys, respectively.

Calibration can be done semi automatically by clicking on an object on the mask image below the sliders. 
The program will pick relevant thresholds for the object you clicked on, adjusting for light and colour. 
This can then be fine tuned by manually changing the sliders.

------
###Installation

#### Linux/DICE

To install the Polygon library, download the [source](https://bitbucket.org/jraedler/polygon2/downloads/Polygon2-2.0.6.zip), navigate inside and execute `python setup.py install --user`.

To install Argparse for python, download [ArgParse](http://argparse.googlecode.com/files/argparse-1.2.1.tar.gz), extract and run `python setup.py install --user`. All done.

The *serial* library is also required.

------
### Vision 

* At the moment OpenCV + Python are being used. A [book](http://programmingcomputervision.com/downloads/ProgrammingComputerVision_CCdraft.pdf) on Computer Vision with OpenCV in Python is a decent starting point about what OpenCV can do.
* A detailed tutorial with examples and use cases can be found [here](https://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_tutorials.html) - going through it can be handy to understand the code
* For OpenCV installation instructions please get in touch with others or have a look at the scripts in *vision/*

------
### Installing OpenCV

#### Linux/DICE
* Download [OpenCV](http://sourceforge.net/projects/opencvlibrary/files/opencv-unix/2.4.8/opencv-2.4.8.zip/download)
* Extract the contents
* When on computers with with video feed, navigate to */disk/scratch/sdp/* or */tmp/* and create a directory for OpenCV
* Copy extracted contents over to this directory
* Create directory `build` and navigate inside
* Execute 
```
cmake -D CMAKE_INSTALL_PREFIX=~/.local ..` or *cmake -D WITH_OPENCL=OFF -D WITH_CUDA=OFF -D BUILD_opencv_gpu=OFF -D BUILD_opencv_gpuarithm=OFF -D BUILD_opencv_gpubgsegm=OFF -D BUILD_opencv_gpucodec=OFF -D BUILD_opencv_gpufeatures2d=OFF -D BUILD_opencv_gpufilters=OFF -D BUILD_opencv_gpuimgproc=OFF -D BUILD_opencv_gpulegacy=OFF -D BUILD_opencv_gpuoptflow=OFF -D BUILD_opencv_gpustereo=OFF -D BUILD_opencv_gpuwarping=OFF -D CMAKE_INSTALL_PREFIX=~/.local ..
```
to install without GPU libraries
* Execute `make` and wait (for quite some time)
* Execute `make install`
* Run `ipython` and do `import cv2`, if all executes fine then you're set.


#### Installing OpenCV on OS X
The problem with installing OpenCV on OS X is because the Python library was installed in the wrong directory. To fix this..:
* Go to *usr/local/share/bin*
* Copy the contents from *python2.7/site-packages* to */Library/Python/2.7/site-packages*

CREDIT: TEAM 7 2014